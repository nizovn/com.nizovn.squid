function MainAssistant() {
};

MainAssistant.prototype.setup = function() {
	this.controller.get('main-title').innerHTML = $L('Squid SSL Bump');
	this.controller.get('version').innerHTML = $L("v" + Mojo.Controller.appInfo.version);
	this.attributes = {
		type : Mojo.Widget.activityButton
	}
	this.buttonmodelstart = {
		label : "Start squid",
		buttonClass : "affirmative",
		disabled: true
	};
	this.controller.setupWidget("StartButton", this.attributes, this.buttonmodelstart);
	this.buttonmodelstop = {
		label : "Stop squid",
		buttonClass : "negative",
		disabled: true
	};
	this.controller.setupWidget("StopButton", this.attributes, this.buttonmodelstop);
	this.checkboxmodel = {
		value: false,
		disabled: true
	};
	this.controller.setupWidget("AutoStartCheckBox",
		this.attributes = {
			trueValue: "on",
			falseValue: "off"
		},
		this.checkboxmodel
	); 
	this.controller.setupWidget("CertificateButton",
		this.attributes = {
			type : Mojo.Widget.activityButton
		},
		this.model = {
			label : "Generate && install certificate",
			disabled: false
		}
	);

	Mojo.Event.listen(this.controller.get("StartButton"), Mojo.Event.tap, this.TapStart.bind(this));
	Mojo.Event.listen(this.controller.get("StopButton"), Mojo.Event.tap, this.TapStop.bind(this));
	Mojo.Event.listen(this.controller.get("AutoStartCheckBox"), Mojo.Event.propertyChange, this.handleCheckboxUpdate.bind(this));
	Mojo.Event.listen(this.controller.get("CertificateButton"), Mojo.Event.tap, this.TapCertificate.bind(this));
};
MainAssistant.prototype.TapStart = function(event) {
	this.buttonmodelstart.disabled = true;
	this.controller.modelChanged(this.buttonmodelstart);
	this.controller.get('StartButton').mojo.activate();
	this.controller.serviceRequest('palm://com.nizovn.squid.c', {
					method: "setState",
					parameters: {"state": "on"},
					onSuccess: this.SuccessOn.bind(this),
					onFailure: function(){}
				});
};
MainAssistant.prototype.TapStop = function(event) {
	this.buttonmodelstop.disabled = true;
	this.controller.modelChanged(this.buttonmodelstop);
	this.controller.get('StopButton').mojo.activate();
	this.controller.serviceRequest('palm://com.nizovn.squid.c', {
					method: "setState",
					parameters: {"state": "off"},
					onSuccess: this.SuccessOff.bind(this),
					onFailure: function(){}
				});
};
MainAssistant.prototype.handleCheckboxUpdate = function(event) {
	var state = event.value;
	this.controller.serviceRequest('palm://com.nizovn.squid.c', {
					method: "setAutoStartState",
					parameters: {"state": state},
					onSuccess: this.SuccessAuto.bind(this),
					onFailure: function(){}
				});
};
MainAssistant.prototype.TapCertificate = function(event) {
	this.controller.get('CertificateButton').mojo.activate();
	this.controller.serviceRequest('palm://com.nizovn.squid.c', {
					method: "updateCertificate",
					parameters: {},
					onSuccess: function() {
						this.controller.get('CertificateButton').mojo.deactivate();
					}.bind(this),
					onFailure: function(){}
				});
}
MainAssistant.prototype.SuccessOn = function(event) {
	this.controller.get('StartButton').mojo.deactivate();
	started = true;
	this.Main();
};
MainAssistant.prototype.SuccessOff = function(event) {
	this.controller.get('StopButton').mojo.deactivate();
	started = false;
	this.Main();
};
MainAssistant.prototype.SuccessAuto = function(event) {
	autoStart = event.state==="on";
	this.Main();
};
MainAssistant.prototype.Failure = function(event) {
	Mojo.log("Failure, results=" + JSON.stringify(event));
	this.Main();
};
MainAssistant.prototype.activate = function(event){
	this.controller.serviceRequest('palm://com.nizovn.squid.c', {
					method: "getStatus",
					parameters: {},
					onSuccess: this.SuccessGetStatus.bind(this),
					onFailure: function(){}
				});
};
var started = undefined;
var autoStart = undefined;
MainAssistant.prototype.SuccessGetStatus = function(event){
	started = event.state==="on";
	autoStart = event.autoStart==="on";
	this.Main();
};
MainAssistant.prototype.Main = function() {
	if (started===true) {
		this.buttonmodelstart.disabled = true;
		this.buttonmodelstop.disabled = false;
	} else if (started===false) {
		this.buttonmodelstart.disabled = false;
		this.buttonmodelstop.disabled = true;
	} else {
		this.buttonmodelstart.disabled = true;
		this.buttonmodelstop.disabled = true;
	}
	if (autoStart===true) {
		this.checkboxmodel.disabled = false;
		this.checkboxmodel.value = "on";
	} else if (autoStart===false) {
		this.checkboxmodel.disabled = false;
		this.checkboxmodel.value = "off";
	} else {
		this.checkboxmodel.disabled = true;
		this.checkboxmodel.value = "off";
	}
	this.controller.modelChanged(this.buttonmodelstart);
	this.controller.modelChanged(this.buttonmodelstop);
	this.controller.modelChanged(this.checkboxmodel);
};
MainAssistant.prototype.deactivate = function(event) {};
MainAssistant.prototype.cleanup = function(event) {
	Mojo.Event.stopListening(this.controller.get("StartButton"), Mojo.Event.tap, this.TapStart.bind(this));
	Mojo.Event.stopListening(this.controller.get("StopButton"), Mojo.Event.tap, this.TapStop.bind(this));
	Mojo.Event.stopListening(this.controller.get("AutoStartCheckBox"), Mojo.Event.propertyChange, this.handleCheckboxUpdate.bind(this));
	Mojo.Event.stopListening(this.controller.get("CertificateButton"), Mojo.Event.tap, this.TapCertificate.bind(this));
};
