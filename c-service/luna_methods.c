/*=============================================================================
 Copyright (C) 2010 WebOS Internals <support@webos-internals.org>
 Copyright (C) 2019 Nikolay Nizov <nizovn@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 =============================================================================*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "luna_service.h"
#include "luna_methods.h"
#include <linux/input.h>
#include <linux/uinput.h>
#include <fcntl.h>

bool getstatus_method(LSHandle* lshandle, LSMessage *message, void *ctx) {
  LSError lserror;
  LSErrorInit(&lserror);

  bool state = (system(APP_PATH "/squid/sbin/squid -k check") == 0);
  bool autoStart = (access(SQUID_FLAGFILE , F_OK) == 0);

  // Local buffer to store the reply
  char reply[MAXLINLEN];

  sprintf(reply, "{\"returnValue\": true, \"state\": \"%s\", \"autoStart\": \"%s\"}",
    (state)?"on":"off", (autoStart)?"on":"off");
  if (!LSMessageReply(lshandle, message, reply, &lserror)) goto error;
  return true;
 error:
  LSErrorPrint(&lserror, stderr);
  LSErrorFree(&lserror);
 end:
  return false;
}

//
// Return the current API version of the service.
// Called directly from webOS, and returns directly to webOS.
//
bool version_method(LSHandle* lshandle, LSMessage *message, void *ctx) {
  LSError lserror;
  LSErrorInit(&lserror);

  if (!LSMessageReply(lshandle, message, "{\"returnValue\": true, \"version\": \"" VERSION "\", \"apiVersion\": \"" API_VERSION "\"}", &lserror)) goto error;

  return true;
 error:
  LSErrorPrint(&lserror, stderr);
  LSErrorFree(&lserror);
 end:
  return false;
}

bool setstate_method(LSHandle* lshandle, LSMessage *message, void *ctx) {
  LSError lserror;
  LSErrorInit(&lserror);

  json_t *object = json_parse_document(LSMessageGetPayload(message));
  json_t *state= json_find_first_label(object, "state");               
  if (state && (state->child->type == JSON_STRING)) {
    char *state_string = state->child->text;
    if (!strcmp(state_string,"on")) {
      system(APP_PATH "/scripts/start.sh");
      if (!LSMessageReply(lshandle, message, "{\"returnValue\": true}", &lserror)) goto error;
      return true;
    } else 
    if (!strcmp(state_string,"off")) {
      system(APP_PATH "/scripts/stop.sh");
      if (!LSMessageReply(lshandle, message, "{\"returnValue\": true}", &lserror)) goto error;
      return true;
    }
  }
  if (!LSMessageReply(lshandle, message,
    "{\"returnValue\": false, \"errorCode\": -1, \"errorText\": \"Invalid or missing state\"}",
    &lserror)) goto error;
  return true;
 error:
  LSErrorPrint(&lserror, stderr);
  LSErrorFree(&lserror);
  return false;
}

bool autoStartState_method(LSHandle* lshandle, LSMessage *message, void *ctx) {
  LSError lserror;
  LSErrorInit(&lserror);

  // Local buffer to store the reply
  char reply[MAXLINLEN];

  json_t *object = json_parse_document(LSMessageGetPayload(message));
  json_t *state= json_find_first_label(object, "state");               
  if (state && (state->child->type == JSON_STRING)) {
    char *state_string = state->child->text;
    sprintf(reply, APP_PATH "/scripts/autostart.sh %s", state_string);
    int ret = system(reply);

    if (!ret) {
      sprintf(reply, "{\"returnValue\": true, \"state\": \"%s\"}", state_string);
      if (!LSMessageReply(lshandle, message, reply, &lserror)) goto error;
      return true;
    }
  }
  if (!LSMessageReply(lshandle, message,
    "{\"returnValue\": false, \"errorCode\": -1, \"errorText\": \"Invalid or missing state\"}",
    &lserror)) goto error;
  return true;
 error:
  LSErrorPrint(&lserror, stderr);
  LSErrorFree(&lserror);
  return false;
}

//
// Called directly from webOS, and returns directly to webOS.
//
bool certificate_method(LSHandle* lshandle, LSMessage *message, void *ctx) {
  LSError lserror;
  LSErrorInit(&lserror);

  system(APP_PATH "/scripts/certificate.sh");
  if (!LSMessageReply(lshandle, message, "{\"returnValue\": true}", &lserror)) goto error;
  return true;
error:
  LSErrorPrint(&lserror, stderr);
  LSErrorFree(&lserror);
  return false;
}
LSMethod luna_methods[] = {
  { "version",		version_method },

  { "updateCertificate",	certificate_method},
  { "setState",	setstate_method},
  { "setAutoStartState",	autoStartState_method},
  { "getStatus",	getstatus_method},

  { 0, 0 }
};

bool register_methods(LSPalmService *serviceHandle, LSError lserror) {
  return LSPalmServiceRegisterCategory(serviceHandle, "/", luna_methods,
           NULL, NULL, NULL, &lserror);
}
