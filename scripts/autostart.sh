FLAGFILE=/media/cryptofs/apps/usr/palm/applications/com.nizovn.squid/squid/etc/autostart.txt
if [ $1 == "on" ] ; then
	touch ${FLAGFILE}
	exit 0
fi
if [ $1 == "off" ] ; then
	rm -f ${FLAGFILE}
	exit 0
fi
exit 1
