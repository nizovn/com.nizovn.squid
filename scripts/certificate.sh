export OPENSSL_CONF=/media/cryptofs/apps/usr/palm/applications/com.nizovn.squid/squid/etc/openssl.cnf
if [ ! -f "${OPENSSL_CONF}" ]; then
	cp /etc/ssl/openssl.cnf "${OPENSSL_CONF}"
	cat >> "${OPENSSL_CONF}" <<EOF
[ req ]
default_bits            = 2048
default_keyfile         = privkey.pem
distinguished_name      = req_distinguished_name
x509_extensions = v3_ca # The extentions to add to the self signed cert

[ req_distinguished_name ]
countryName                     = Country Name (2 letter code)
countryName_default             = AU
countryName_min                 = 2
countryName_max                 = 2

stateOrProvinceName             = State or Province Name (full name)
stateOrProvinceName_default     = Some-State

localityName                    = Locality Name (eg, city)

0.organizationName              = Organization Name (eg, company)
0.organizationName_default      = Internet Widgits Pty Ltd

# we can do this but it is not needed normally :-)
#1.organizationName             = Second Organization Name (eg, company)
#1.organizationName_default     = World Wide Web Pty Ltd

organizationalUnitName          = Organizational Unit Name (eg, section)
#organizationalUnitName_default =

commonName                      = Common Name (e.g. server FQDN or YOUR name)
commonName_max                  = 64

emailAddress                    = Email Address
emailAddress_max                = 64

[ v3_req ]
basicConstraints = CA:FALSE
keyUsage = nonRepudiation, digitalSignature, keyEncipherment
[ v3_ca ]
keyUsage = cRLSign, keyCertSign
EOF

fi

openssl req -new -newkey rsa:2048 -sha256 -days 365 -nodes -x509 -extensions v3_ca \
  -keyout /tmp/squid.pem  -out /tmp/squid.pem -subj "/C=AU/ST=Some-State/O=squid/CN=squid"
luna-send -a com.nizovn.squid.c -n 1 luna://com.palm.certificatemanager/addcertificate '{"certificateFilename": "/tmp/squid.pem"}'
mv /tmp/squid.pem /media/cryptofs/apps/usr/palm/applications/com.nizovn.squid/squid/etc/squid.pem
luna-send -n 1 luna://com.palm.applicationManager/launch '{"id":"com.palm.app.certificate"}'
